use std::{env, process::exit};
use log::{error, warn};
use std::path::Path;

mod fs;
use fs::FS;

fn main() {
    env_logger::init();
    let arg1 = env::args_os().nth(1);
    let mountpoint = if arg1.is_some() {
        arg1.unwrap()
    } else {
        error!("Usage: [prog] MOUNTPOINT");
        exit(1)
    };

    if ! Path::new(&mountpoint).is_dir() {
        warn!("Mountpoint does not exist, creating it");
        let err = std::fs::create_dir(&mountpoint);
        if err.is_err() {
            error!("Failed to create mountpoint: {}", err.unwrap_err());
            exit(1)
        }
    }
    let fs =FS{ data: &mut [0; 64] } ;
    fuse::mount(fs, &mountpoint, &[]).unwrap();
}
