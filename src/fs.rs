use std::ffi::OsStr;
use fuse::*;
use log::info;
use time::Timespec;
use libc::ENOENT;

const NO_TIME: Timespec = Timespec { sec: 1, nsec: 0 };

const DIR_ATTR: FileAttr = FileAttr {
    ino: 1,
    size: 0,
    blocks: 0,
    atime: NO_TIME,
    mtime: NO_TIME,
    ctime: NO_TIME,
    crtime: NO_TIME,
    kind: FileType::Directory,
    perm: 0o755,
    nlink: 2,
    uid: 501,
    gid: 20,
    rdev: 0,
    flags: 0,
};


const F_ATTRS: FileAttr = FileAttr {
    ino: 2,
    size: 13,
    blocks: 1,
    atime: NO_TIME,
    mtime: NO_TIME,
    ctime: NO_TIME,
    crtime: NO_TIME,
    kind: FileType::RegularFile,
    perm: 0o644,
    nlink: 1,
    uid: 501,
    gid: 20,
    rdev: 0,
    flags: 0,
};


pub struct FS<'a> {
    pub data: &'a mut [u8],
}

impl Filesystem for FS<'_> {

    fn lookup(&mut self, _req: &Request, parent: u64, name: &OsStr, reply: ReplyEntry) {
        if parent == 1 && name.to_str() == Some("hello.txt") {
            reply.entry(&NO_TIME, &F_ATTRS, 0);
        } else {
            reply.error(ENOENT);
        }
    }

    fn getattr(&mut self, _req: &Request, ino: u64, reply: ReplyAttr) {
        match ino {
            1 => reply.attr(&NO_TIME, &DIR_ATTR),
            2 => reply.attr(&NO_TIME, &F_ATTRS),
            _ => reply.error(ENOENT),
        }
    }

    fn setattr(&mut self, _req: &Request, ino: u64, _mode: Option<u32>, _uid: Option<u32>, _gid: Option<u32>, _size: Option<u64>, _atime: Option<Timespec>, _mtime: Option<Timespec>, _fh: Option<u64>, _crtime: Option<Timespec>, _chgtime: Option<Timespec>, _bkuptime: Option<Timespec>, _flags: Option<u32>, reply: ReplyAttr) {
        match ino {
            1 => reply.attr(&NO_TIME, &DIR_ATTR),
            2 => reply.attr(&NO_TIME, &F_ATTRS),
            _ => reply.error(ENOENT),
        }
    }

    fn open(&mut self, _req: &Request, _ino: u64, flags: u32, reply: ReplyOpen) {
        if flags & 1 == 0 {
            reply.opened(0, flags);
            return;
        }

        let append = flags as i32 & libc::O_APPEND == libc::O_APPEND;
        if append {
            info!("Append mode.");
            reply.opened(0, flags);
            return;
        }else {
            info!("Write mode.");
            self.data.fill(0);
            reply.opened(0, flags);
            return;
        }
    }

    fn read(&mut self, _req: &Request, ino: u64, _fh: u64, offset: i64, _size: u32, reply: ReplyData) {
        if ino != 2 {
            reply.error(ENOENT);
            return;
        }

        info!("{}", String::from_utf8(self.data.to_vec()).unwrap());
        reply.data(&self.data[offset as usize..]);
    }

    fn write(&mut self, _req: &Request, ino: u64, _fh: u64, offset: i64, data: &[u8], _flags: u32, reply: ReplyWrite) {
        if ino != 2 {
            reply.error(ENOENT);
            return;
        }
        info!("Write at {}", offset);

        let mut written = 0;
        for i in offset as usize..offset as usize + data.len() {
            if i >= self.data.len() {
                break;
            }

            self.data[i] = data[written];
            written += 1;
        }
        
        info!("{}", String::from_utf8(self.data.to_vec()).unwrap());
        reply.written(written.clone() as u32);
    }

    fn flush(&mut self, _req: &Request, _ino: u64, _fh: u64, _lock_owner: u64, reply: ReplyEmpty) {
        reply.ok();
    }

    fn fsync(&mut self, _req: &Request, _ino: u64, _fh: u64, _datasync: bool, reply: ReplyEmpty) {
        reply.ok();
    }

    fn readdir(&mut self, _req: &Request, ino: u64, _fh: u64, offset: i64, mut reply: ReplyDirectory) {
        if ino != 1 {
            reply.error(ENOENT);
            return;
        }

        let entries = vec![
            (1, FileType::Directory, "."),
            (1, FileType::Directory, ".."),
            (2, FileType::RegularFile, "hello.txt"),
        ];

        for (i, entry) in entries.into_iter().enumerate().skip(offset as usize) {
            // i + 1 means the index of the next entry
            reply.add(entry.0, (i + 1) as i64, entry.1, entry.2);
        }
        reply.ok();
    }
}
